package info.grade.web;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Purpose: Directs server to correct page and adds needed information to each
 * page.
 */
@Controller
public class OtherController {

    @RequestMapping("/other/error")
    public String showError(Model model, HttpSession session) {
        return "other/error";
    }

    @RequestMapping("/other/unauthorizedaccess")
    public String showUnauthorizedAccess(Model model, HttpSession session) {
        return "other/unauthorizedaccess";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model, HttpSession session) {
        return "other/help";
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model, HttpSession session) {
        return "other/about";

    }

    @RequestMapping("/PDFViewer")
    public String showPDFViewer(Model model, HttpSession session) {
        return "other/PDFViewer";
    }

    @RequestMapping("/other/welcome")
    public String showWelcome(Model model, HttpSession session) {
        return "other/welcome";
    }

    @RequestMapping("/")
    public String showMainPage(Model model, HttpSession session) {
        return "other/welcome";
    }
}
